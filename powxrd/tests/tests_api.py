from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from ..models import CSV
from django.core.files.uploadedfile import SimpleUploadedFile
import os

class CSVApiTestCase(APITestCase):
    def test_list_csv(self):
        url = reverse('powxrd:csv_list_create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ScanApiTestCase(APITestCase):
    def test_list_scan(self):
        url = reverse('powxrd:scan_list_create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class PredictionsApiTestCase(APITestCase):
    def test_list_predictions(self):
        url = reverse('powxrd:predictions_list_create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class EngineAPITestCase(APITestCase):
    def setUp(self):
        # Create a test CSV file
        content = b"Angle,Intensity\n20,100\n25,500\n30,200\n"
        self.csv_file = SimpleUploadedFile("test.csv", content, content_type="text/csv")

        # Create a CSV instance and save it
        csv_instance = CSV(filename=self.csv_file)
        csv_instance.save()

        # Set session variables
        self.client.session['csv_path'] = os.path.join("scan_upload", str(csv_instance.filename))
        self.client.session['wavelength'] = 1.54184
        self.client.session.save()

    def test_engine_api(self):
        # Send a GET request to EngineAPIView
        response = self.client.get(reverse('powxrd:engine_api'))

        # Check if the response has a status code of 200 (OK)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response data has the expected keys
        self.assertTrue('labels' in response.data)
        self.assertTrue('probabilities' in response.data)
        self.assertTrue('Q' in response.data)
        self.assertTrue('intensities' in response.data)
