from django.test import TestCase
from django.core.files.uploadedfile import SimpleUploadedFile
import pytest
from ..models import Scan, Predictions, CSV

@pytest.fixture
def sample_scan_data():
    return {
        'name': 'Scan 1',
        'description': 'This is a sample scan',
        'angles': [0, 45, 90, 135, 180],
        'intensities': [1, 2, 3, 4, 5],
    }

@pytest.mark.django_db
def test_create_scan(sample_scan_data):
    scan = Scan.objects.create(**sample_scan_data)
    assert scan.id is not None
    assert scan.name == 'Scan 1'
    assert scan.description == 'This is a sample scan'
    assert scan.intensities == [1, 2, 3, 4, 5]
    assert scan.angles == [0, 45, 90, 135, 180]

@pytest.mark.django_db
def test_update_scan(sample_scan_data):
    scan = Scan.objects.create(**sample_scan_data)
    new_data = {
        'name': 'Scan 2',
        'description': 'This is an updated scan',
        'angles': [0, 40, 95, 125, 180],  # angles should not be updated
        'intensities': [6, 7, 8, 9, 10],
    }
    scan.name = new_data['name']
    scan.description = new_data['description']
    scan.time_series = new_data['time_series']
    scan.save()
    updated_scan = Scan.objects.get(id=scan.id)
    assert updated_scan.name == 'Scan 2'
    assert updated_scan.description == 'This is an updated scan'
    assert updated_scan.angles == [0, 45, 90, 135, 180]
    assert updated_scan.intensities == [6, 7, 8, 9, 10]

@pytest.mark.django_db
def test_delete_scan(sample_scan_data):
    scan = Scan.objects.create(**sample_scan_data)
    scan.delete()
    with pytest.raises(Scan.DoesNotExist):
        Scan.objects.get(id=scan.id)


@pytest.mark.django_db
def test_predictions_model():
    # Create a new Predictions object
    predictions = Predictions.objects.create(
        labels=['Label 1', 'Label 2', 'Label 3'],
        probabilities=[0.5, 0.3, 0.2]
    )

    # Check that the object was created correctly
    assert predictions.labels == ['Label 1', 'Label 2', 'Label 3']
    assert predictions.probabilities == [0.5, 0.3, 0.2]

    # Test the __str__ method
    assert str(predictions) == f"Predictions is: {predictions.id}"



@pytest.mark.django_db
def test_csv_model():
    # Create a new CSV object
    csv_file = SimpleUploadedFile("file.csv", b"file_contents")
    csv = CSV.objects.create(filename=csv_file)

    # Check that the object was created correctly
    assert csv.filename.name == 'scan_upload/file.csv'
    assert csv.timestamp is not None
    assert csv.activated == False

    # Test the __str__ method
    assert str(csv) == f"File id: {csv.id}"