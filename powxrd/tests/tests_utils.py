from ..utils import normalise, twotheta_to_q, process_scan
import numpy as np
import torch
import scipy.signal
import os
import pandas as pd
import pytest
from models import Scan

""""Spectra normalization test"""

def test_normalise_numpy():
    # Create a numpy array with some data
    arr = np.array([1, 2, 3, 4, 5])

    # Normalize the array using the normalise function
    arr_normed = normalise(arr)

    # Check that the output is a numpy array with values between 0 and 1
    assert isinstance(arr_normed, np.ndarray)
    assert np.all(arr_normed >= 0)
    assert np.all(arr_normed <= 1)

def test_normalise_torch():
    # Create a PyTorch tensor with some data
    tensor = torch.tensor([1, 2, 3, 4, 5])

    # Normalize the tensor using the normalise function
    tensor_normed = normalise(tensor)

    # Check that the output is a PyTorch tensor with values between 0 and 1
    assert isinstance(tensor_normed, torch.Tensor)
    assert torch.all(tensor_normed >= 0)
    assert torch.all(tensor_normed <= 1)

"""angle to Q tests"""

def test_twotheta_to_q_wavelength_default():
    """
    Test the `twotheta_to_q` function with default wavelength value.
    """
    # Test input and expected output
    two_theta = 45
    expected_output = 1.7755767144981084

    # Test function output
    assert np.isclose(twotheta_to_q(two_theta), expected_output)

def test_twotheta_to_q_wavelength_custom():
    """
    Test the `twotheta_to_q` function with custom wavelength value.
    """
    # Test input and expected output
    two_theta = 30
    wavelength = 0.5
    expected_output = 5.046121593929943

    # Test function output
    assert np.isclose(twotheta_to_q(two_theta, wavelength), expected_output)



@pytest.fixture
def csv_path():
    csv_path = os.path.join(os.path.dirname(__file__), 'test_scan.csv')
    return csv_path


def test_process_scan(csv_path):
    space_group_features, scan_id = process_scan(csv_path)

    # Test that the space group features have the expected shape
    assert space_group_features.shape == (10,)

    # Test that the Scan object was created and saved with the expected id
    scan = Scan.objects.get(id=scan_id)
    assert scan is not None

    # Test that the intensities were normalized as expected
    df = pd.read_csv(csv_path, delimiter=',')
    columns = df.columns
    I = df[columns[1]].to_numpy()
    I_normed = normalise(I)
    assert np.allclose(scan.intensities, I_normed.tolist())

    # Test that peaks were detected and space group features were computed as expected
    peaks = scipy.signal.find_peaks(I_normed)
    first_ten_angles = df[columns[0]].to_numpy()[peaks[0][:10]]
    expected_space_group_features = np.asarray([twotheta_to_q(theta) for theta in first_ten_angles])
    assert np.allclose(space_group_features, expected_space_group_features)