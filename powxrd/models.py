from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField

class CSV(models.Model):
    filename = models.FileField(upload_to='scan_upload/', related_name='uploaded_files')
    timestamp = models.DateTimeField(auto_now_add=True)
    activated = models.BooleanField(default=False)

    def __str__(self):
        return f"File id: {self.id}"

class Scan(models.Model):
    angles = ArrayField(models.FloatField())
    intensities = ArrayField(models.FloatField())
    timestamp = models.DateTimeField(auto_now_add=True)
    csv = models.ForeignKey(CSV, on_delete=models.CASCADE)

    def __str__(self):
        return f"Scan is: {self.id}"
    
class Predictions(models.Model):
    labels = ArrayField(models.CharField(max_length=100))
    probabilities = ArrayField(models.FloatField())
    csv = models.ForeignKey(CSV, on_delete=models.CASCADE)

    def __str__(self):
        return f"Predictions is: {self.id}"
