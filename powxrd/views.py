import os
from django.conf import settings
from django.views.generic.edit import FormView
from .models import CSV, Scan
from .forms import CSVForm

# Create your views here.


from django.views.generic.edit import FormView
from django.urls import reverse_lazy

class ListView(FormView):
    template_name = 'list.html'
    form_class = CSVForm
    success_url = reverse_lazy('powxrd:list')

    def form_valid(self, form):
        request = self.request
        filename_ = request.FILES['csvfile'].name
        path = os.path.join(settings.BASE_DIR, 'scan_upload')
        with open(os.path.join(path, filename_), 'wb+') as destination:
            for chunk in request.FILES['csvfile'].chunks():
                destination.write(chunk)

        form.instance.filename = request.FILES['csvfile']
        form.save()

        request.session['wavelength'] = form.cleaned_data['wavelength']

        return super().form_valid(form)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        csv_path = ''
        csv_path1 = ''
        documents = CSV.objects.all()
        for document in documents:
            csv_path = document.filename.name
            csv_path1 = '/' + csv_path
            document.delete()

        context['csv_path1'] = csv_path1
        context['documents'] = documents
        return context



from .utils import  process_data
from django.views import View
from django.http import JsonResponse

class EngineView(View):
    def get(self, request, *args, **kwargs):
        spectra_path = os.path.join(str(settings.BASE_DIR), request.session['csv_path'])
        wavelength = request.session['wavelength']
        request.session.pop('csv_path', None)
        request.session.pop('wavelength', None)
        request.session.modified = True
        response_data = process_data(spectra_path, wavelength)

        return JsonResponse(response_data)


from rest_framework import generics
from .models import Scan, CSV
from .serializers import ScanSerializer, ResponseDataSerializer
from rest_framework.response import Response


class ScanListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ScanSerializer

    def get_queryset(self):
        csv_id = self.kwargs['csv_id']
        return Scan.objects.filter(csv__id=csv_id)

    def perform_create(self, serializer):
        csv_id = self.kwargs['csv_id']
        csv = CSV.objects.get(id=csv_id)
        serializer.save(csv=csv)


class EngineAPIView(generics.GenericAPIView):
    serializer_class = ResponseDataSerializer

    def get(self, request, *args, **kwargs):
        spectra_path = os.path.join(str(settings.BASE_DIR), request.session['csv_path'])
        wavelength = request.session['wavelength']
        response_data = process_data(spectra_path, wavelength)

        serializer = self.get_serializer(data=response_data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)

