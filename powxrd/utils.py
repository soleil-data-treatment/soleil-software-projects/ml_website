import numpy as np
import pandas as pd
import torch
import joblib
import scipy
import scipy.signal
from .models import Scan, Predictions
from pymatgen.symmetry.groups import sg_symbol_from_int_number
import rpy2.robjects as robjects
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter

# Enable R to Python dataframe conversion
pandas2ri.activate()


#CLASS_INDEX_PATH = 'static/material_labels.csv'
MODEL_PATH_SG = "static/exrt_sgr_model.rds"


def load_ranger_model(model_path):
    read_rds = robjects.r['readRDS']
    return read_rds(model_path)

def predict_with_ranger(model, input_dataframe):
    # Convert the input dataframe to an R dataframe
    with localconverter(robjects.default_converter + pandas2ri.converter):
        r_dataframe = robjects.conversion.py2rpy(input_dataframe)

    # Make predictions using the ranger model
    predict_function = robjects.r['predict']
    predictions = predict_function(model, r_dataframe)
    
    # Convert the R predictions to a Python pandas dataframe
    with localconverter(robjects.default_converter + pandas2ri.converter):
        pandas_predictions = robjects.conversion.rpy2py(predictions)

    return pandas_predictions

def normalise(spectra):
    """
    Normalizes the input spectra by scaling its values between 0 and 1.

    Parameters:
    -----------
    spectra: numpy.ndarray or torch.Tensor
        The spectra to be normalized.

    Returns:
    --------
    numpy.ndarray or torch.Tensor
        The normalized spectra.

    Raises:
    -------
    TypeError:
        If the input spectra is not of type numpy.ndarray or torch.Tensor.
    """
    if type(spectra) is np.ndarray:
        max_I = np.max(spectra)
        min_I = np.min(spectra)
    elif type(spectra) is torch.Tensor:
        max_I = torch.max(spectra)
        min_I = torch.min(spectra)
    spectra_normed = (spectra - min_I) / (max_I - min_I)
    return spectra_normed


def twotheta_to_q(two_theta, wavelength = 1.54184):
    """
    Calculates the scattering vector (q) in inverse angstroms from the two-theta angle 
    (in degrees) and the wavelength of incident X-rays (in angstroms). 

    Parameters:
    -----------
    two_theta: float
        The two-theta angle in degrees.
    wavelength: float, optional
        The wavelength of incident X-rays in angstroms. Default value is 1.54184, 
        which corresponds to the wavelength of Cu K-alpha radiation commonly used 
        in X-ray crystallography.

    Returns:
    --------
    float
        The scattering vector (q) in inverse angstroms.
    """
    q = 4*np.pi*(np.sin(np.deg2rad(two_theta/2)))/wavelength
    return q


def process_scan(scan_path, wavelength):
    """
    Reads the intensity and angle data from a CSV file located at `scan_path`, 
    creates a new `Scan` object with the normalized intensity and angle data, 
    saves it to the database, and extracts important features such as the 
    first ten peaks and corresponding q-values.

    Parameters:
    -----------
    scan_path : str
        The path to the CSV file containing the scan data.

    Returns:
    --------
    tuple
        A tuple containing two items:
        - A numpy array of q-values corresponding to the first ten peaks in the 
          normalized intensity data.
        - The ID of the newly saved `Scan` object.
    """
    df = pd.read_csv(scan_path, delimiter=',')
    columns = df.columns
    I = df[columns[1]].to_numpy()
    angles = df[columns[0]].to_numpy()
    I = normalise(I)
    scan = Scan(angles = angles.tolist(), intensities = I.tolist())
    scan.save()
    scan_id = scan.id
    peaks = scipy.signal.find_peaks(I)
    first_ten_angles = angles[peaks[0][:10]]
    sg_features = first_ten_angles.tolist()
    space_group_features = np.asarray([twotheta_to_q(theta, wavelength) for theta in sg_features])
    return space_group_features, scan_id


def get_predictions(features,  top_k=5):
    """
    Uses a trained R model (ranger package) to predict the class labels and probabilities 
    for a set of input features, saves the top predictions to the database as a `Predictions` 
    object, and returns the ID of the newly saved object.

    Parameters:
    -----------
    features : numpy.ndarray
        A 1D numpy array containing the input features to be classified.
    top_k : int, optional
        The number of top predictions to return. Default is 5.

    Returns:
    --------
    int
        The ID of the newly saved `Predictions` object.

    Raises:
    -------
    ValueError
        If `features` is not a 1D numpy array.

    Notes:
    ------
    - The R model used to make predictions is assumed to be a trained ranger model
      loaded from an RDS file.
    - The `Predictions` object is saved to the database using the Django ORM `save` method.
    """
    if len(features.shape) != 1:
        raise ValueError("Input features must be a 1D numpy array.")

    # Convert the numpy array to an R dataframe
    r_vector = robjects.FloatVector(features)
    r_dataframe = robjects.r['data.frame'](column_name=r_vector)

    # Load the R model
    r_load_model = robjects.r['readRDS']
    r_model = r_load_model('exrt_sgr_model.rds')

    # Call the predict_with_ranger function on the R dataframe
    predict_with_ranger = robjects.r['predict_with_ranger']
    preds_r = predict_with_ranger(r_dataframe, r_model)

    # Convert the R predictions to a numpy array
    with localconverter(robjects.default_converter + pandas2ri.converter):
        preds = np.array(preds_r)

    # Get the top predictions
    top_k = np.argsort(-preds).squeeze()[:top_k]
    top_probas = preds[0, top_k]

    # Save the predictions as a `Predictions` object
    predictions = Predictions(labels = top_k.tolist(), probabilities = top_probas.tolist())
    predictions.save()
    preds_id = predictions.id

    return preds_id



def get_space_group_labels(labels):
    """
    Convert numerical space group labels to space group symbols using pymatgen.

    Args:
        labels (list): A list of numerical space group labels.

    Returns:
        group_labels: A list of space group symbols corresponding to the input labels.
    """
    group_labels = []
    for int_number in labels:
        # Use the sg_symbol_from_int_number() function to get the space group symbol for each integer number
        symbol = sg_symbol_from_int_number(int(int_number))
        group_labels.append(symbol)
    return group_labels


def process_data(spectra_path, wavelength):
    """
    Process the XRD data from a CSV file and return the original scan and space group predictions.

    This function reads the XRD data from the given CSV file, performs required calculations,
    and generates the original scan and space group predictions.

    Args:
        csv_path (str): The file path to the CSV file containing the XRD data.
        wavelength (float): The wavelength value used for calculations.

    Returns:
        tuple: A tuple containing two dictionaries:
            - A dictionary containing the processed original scan data with keys:
                - 'Q': A list of q vector values.
                - 'intensities': A list of intensity values.
            - A dictionary containing the space group predictions with keys:
                - 'labels': A list of space group labels.
                - 'probabilities': A list of probabilities for each space group.
    """
    try:
        features, scan_id = process_scan(spectra_path, wavelength)
        pred_id = get_predictions(features)
        predictions = Predictions.objects.get(id=pred_id)
        scan = Scan.objects.get(id=scan_id)
        labels = predictions.labels
        probabilities = predictions.probabilities
        space_group_labels = get_space_group_labels(labels)

        response_data = {
            'labels': space_group_labels,
            'probabilities': probabilities,
            'Q': [twotheta_to_q(theta, wavelength) for theta in scan.angles],
            'intensities': scan.intensities
        }
    except FileNotFoundError:
        response_data = {
            'labels': None,
            'probabilities': None,
            'Q': None,
            'intensities': None
        }
    
    torch.cuda.empty_cache()

    return response_data
