from django.contrib import admin
from .models import CSV, Scan, Predictions
# Register your models here.


admin.site.register(CSV)
admin.site.register(Scan)
admin.site.register(Predictions)