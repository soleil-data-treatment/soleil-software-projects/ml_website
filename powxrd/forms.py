from django.forms import ModelForm
from .models import CSV

class CSVForm(ModelForm):
    class Meta:
        model = CSV
        fields = ['csvfile', 'wavelength']
