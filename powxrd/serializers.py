from rest_framework import serializers
from .models import CSV, Scan, Predictions

class CSVSerializer(serializers.ModelSerializer):
    class Meta:
        model = CSV
        fields = '__all__'

class ScanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scan
        fields = '__all__'

class PredictionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Predictions
        fields = '__all__'
     
class ResponseDataSerializer(serializers.Serializer):
    labels = serializers.ListField(child=serializers.CharField())
    probabilities = serializers.ListField(child=serializers.FloatField())
    Q = serializers.ListField(child=serializers.FloatField())
    intensities = serializers.ListField(child=serializers.FloatField())