from django.urls import path
from . import views
app_name = 'powxrd'

from django.urls import path
from . import views

app_name = 'powxrd'

urlpatterns = [
    path('', views.PowxrdListView.as_view(), name='list'),
    path('engine/', views.EngineView.as_view(), name='engine'),
    path('api/csv/', views.CSVListCreate.as_view(), name='csv_list_create'),
    path('api/scan/', views.ScanListCreate.as_view(), name='scan_list_create'),
    path('api/predictions/', views.PredictionsListCreate.as_view(), name='predictions_list_create'),
    path('engine/', views.EngineAPIView.as_view(), name='engine'),
]
