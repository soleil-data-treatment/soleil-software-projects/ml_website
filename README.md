# GRADES ML Model Deployment Website


## Description
The GRADES ML Model Deployment Website is a Django-based web application that allows users to deploy machine learning models that have been investigated by the GRADES team. The GRADES team is part of the experiments division of Soleil one whoes goals, is to investigate the reliability and generalization performance of machine learning models, with a focus on deep learning models.

This website is designed to make it easy for users to deploy the models investigated by the GRADES team, and to provide a platform for comparing the performance of different models. The website provides a simple interface for uploading input data and running the models, and it displays the output of the models in a user-friendly format.

The GRADES ML Model Deployment Website is designed to be flexible and extensible, allowing new models to be added easily as they are investigated by any beamline team. The website is built using Django, a popular Python web framework, and it leverages a number of other open-source technologies for model training and deployment, such as TensorFlow, Pytorch and Docker.

If you are interested in deploying machine learning models that have been investigated by the GRADES team, or if you are a researcher investigating the reliability and generalization performance of machine learning models for your beamline use, the GRADES ML Model Deployment Website is a valuable resource that can help you test and compare models quickly and easily.



## installation (first time use)

### Prerquisites:

As of today the following procedure is designed for the follwing system:
- Debian 11
- Has internet connections 
- You have sudo rights

### PART1: Installing postgres DBMS
sudo apt update

sudo apt install postgresql postgresql-contrib

##### Check if postgres is up and working using:

sudo systemctl status postgresql

##### If its not up. Start it and ensure it stays up if you reboot:

sudo systemctl start postgresql

sudo systemctl enable postgresql

### PART2: Creating the roles and tables
sudo su - postgres

psql

CREATE DATABASE grades01database;

CREATE USER dbuser WITH PASSWORD 'Utilisateur1';

GRANT ALL PRIVILEGES ON DATABASE grades01database TO dbuser;

ALTER DATABASE grades01database SET timezone TO 'CET';

\q

exit

### PART3: Setting up the Django project
sudo apt install python3-psycopg2

clone this repository

cd ml_website

python manage.py makemigrations powxrdapp     (**important!**: for each new app added you need a seperate makemigrations)

python manage.py migrate


### PART4: Launching the website

python manage.py runserver

This will run the app on port 8000

Open you browser and go to:

http://localhost:8000/


you can choose another port for example

python manage.py runserver 8080

Adjust the url accordingly.


PS: we are currently working on scripts that automate all of the previous steps but its important to keep the manual procedure for anyone to see.


## Contributing

We welcome contributions from other beamline scientists who are interested in using or improving the GRADES ML Model Deployment Website. If you have ideas for new features, improvements to existing features, or bug fixes, you can submit a pull request to our repository on GitLab.

To make a contribution, follow these steps:

1. Fork the repository on GitLab and create a new branch for your changes.

2. Make your changes to the code, following the existing coding style and conventions. Be sure to test your changes thoroughly to ensure they work as expected.

3. Commit your changes and push them to your forked repository on GitLab.

4. Submit a pull request from your branch to the main branch of the GRADES ML Model Deployment Website repository.

5. Wait for feedback from the GRADES team, and make any necessary changes based on the feedback.

6. Once your changes have been reviewed and approved, they will be merged into the main branch of the repository.

By contributing to the GRADES ML Model Deployment Website, you can help make it a more useful and valuable resource for other beamline scientists. We appreciate any and all contributions, and we look forward to working with you to improve the website.







## Contact
If you have any questions, feedback, or issues with the GRADES ML Model Deployment Website, please don't hesitate to get in touch with us. You can reach the maintainers of the website by email at anass.bellachehab@synchrotron-soleil.fr

We also welcome contributions and suggestions from other beamline scientists who are interested in using or improving the website. If you have an idea for a new feature, improvement, or bug fix, please submit a pull request or issue on our GitLab repository.

Thank you for using the GRADES ML Model Deployment Website, and we look forward to hearing from you!
