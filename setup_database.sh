#!/bin/bash
set -e

# Update package lists
echo "Updating package lists..."
sudo apt update

# Install PostgreSQL and required packages
echo "Installing PostgreSQL and required packages..."
sudo apt install -y postgresql postgresql-contrib


# Enable and start PostgreSQL service
echo "Enabling and starting PostgreSQL service..."
sudo systemctl enable postgresql
sudo systemctl start postgresql

# Set PostgreSQL variables
DB_NAME="grades01database"
DB_USER="dbuser"
DB_PASSWORD="Utilisateur1"

# Create database and user
echo "Creating database and user..."
sudo -u postgres psql -c "CREATE DATABASE ${DB_NAME};"
sudo -u postgres psql -c "CREATE USER ${DB_USER} WITH PASSWORD '${DB_PASSWORD}';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE ${DB_NAME} TO ${DB_USER};"
sudo -u postgres psql -c "ALTER DATABASE ${DB_NAME} SET timezone TO 'UTC';"

echo "PostgreSQL setup complete!"
